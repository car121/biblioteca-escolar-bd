USE [Biblioteca]
GO
/****** Object:  Table [dbo].[Libro]    Script Date: 04/11/2019 11:38:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Libro](
	[Nombre] [nvarchar](50) NULL,
	[Autor] [nvarchar](50) NULL,
	[Categoria] [nvarchar](50) NULL,
	[Editorial] [nvarchar](50) NULL,
	[Año] [int] NULL,
	[Existencia] [int] NULL,
	[Img] [nvarchar](200) NULL,
	[Id_Libro] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id_Libro] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuarios]    Script Date: 04/11/2019 11:38:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios](
	[Nombre] [nvarchar](50) NULL,
	[ApellidoP] [nvarchar](50) NULL,
	[ApellidoM] [nvarchar](50) NULL,
	[NomUsu] [nvarchar](50) NULL,
	[Contrasena] [nvarchar](50) NULL,
	[CantLibros] [int] NULL,
	[Administrador] [bit] NOT NULL,
	[IdUsuario] [int] IDENTITY(1,1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdUsuario] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Prestamos]    Script Date: 04/11/2019 11:38:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Prestamos](
	[Fecha] [nvarchar](50) NOT NULL,
	[IdPrestamo] [int] IDENTITY(1,1) NOT NULL,
	[IdUsuario] [int] NOT NULL,
	[Id_Libro] [int] NOT NULL,
	[NombreLibro] [nvarchar](100) NULL,
	[NombreUsuario] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdPrestamo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[EliminaUsuario]    Script Date: 04/11/2019 11:38:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[EliminaUsuario]

@idusuario int

as
delete from Usuarios where IdUsuario=@idusuario
GO
/****** Object:  StoredProcedure [dbo].[EliminarLibros]    Script Date: 04/11/2019 11:38:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[EliminarLibros]

@id_libro int

as 

delete from Libro where Id_Libro=@id_libro
GO
/****** Object:  StoredProcedure [dbo].[ActualizaLibros]    Script Date: 04/11/2019 11:38:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[ActualizaLibros]

@id_libro int, @nombre nvarchar(50),@autor nvarchar(50),@categoria nvarchar(50),@editorial nvarchar(50),@año int,@existencia int, @img nvarchar(200)

as

if NOT EXISTS (Select Id_Libro FROM Libro WHERE Id_Libro=@id_libro)

insert into Libro(Nombre,Autor,Categoria,Editorial,Año,Existencia,Img)

values(@nombre,@autor,@categoria,@editorial,@año,@existencia,@img)


else


update Libro set Nombre=@nombre, Autor=@autor, Categoria=@categoria , Editorial=@editorial, Año=@año,Existencia=@existencia,Img=@img where Id_Libro=@id_libro
GO
/****** Object:  StoredProcedure [dbo].[ActualizaUsuarios]    Script Date: 04/11/2019 11:38:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ActualizaUsuarios]

@idusuario int , @nombre nvarchar(50),@apellidop nvarchar(50),@apellidom nvarchar(50),@nomusu nvarchar(50), @contrasena nvarchar(50), @cantlibros int,@administrador bit

as

if NOT EXISTS (SELECT IdUsuario From Usuarios where IdUsuario=@idusuario)
insert into Usuarios(Nombre,ApellidoP,ApellidoM,NomUsu,Contrasena,CantLibros,Administrador)
values(@nombre,@apellidop,@apellidom,@nomusu,@contrasena,@cantlibros,@administrador)

else 

update Usuarios set Nombre=@nombre,ApellidoP=@apellidop,ApellidoM=@apellidom,NomUsu=@nomusu,Contrasena=@contrasena,CantLibros=@cantlibros,Administrador=@administrador where IdUsuario=@idusuario
GO
/****** Object:  StoredProcedure [dbo].[ActualizaPrestamo]    Script Date: 04/11/2019 11:38:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ActualizaPrestamo]

@idusuario int , @idlibro int, @fecha nvarchar(50), @nombrelibro nvarchar(100), @nombreusuario nvarchar(100),@idprestamo int

as

if NOT EXISTS (SELECT IdPrestamo FROM Prestamos where IdPrestamo=@idprestamo)
insert into Prestamos(IdUsuario,Id_Libro,Fecha,NombreLibro,NombreUsuario) values (@idusuario,@idlibro,@fecha,@nombrelibro,@nombreusuario)

else

update Prestamos set IdUsuario=@idusuario,Id_Libro=@idlibro,Fecha=@fecha,NombreLibro=@nombrelibro,NombreUsuario=@nombreusuario Where IdPrestamo=@idprestamo
GO
/****** Object:  StoredProcedure [dbo].[EliminaPrestamo]    Script Date: 04/11/2019 11:38:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[EliminaPrestamo]

@idprestamo int

as

delete from Prestamos where IdPrestamo=@idprestamo
GO
/****** Object:  ForeignKey [FK_Prestamos_Libro]    Script Date: 04/11/2019 11:38:07 ******/
ALTER TABLE [dbo].[Prestamos]  WITH CHECK ADD  CONSTRAINT [FK_Prestamos_Libro] FOREIGN KEY([Id_Libro])
REFERENCES [dbo].[Libro] ([Id_Libro])
GO
ALTER TABLE [dbo].[Prestamos] CHECK CONSTRAINT [FK_Prestamos_Libro]
GO
/****** Object:  ForeignKey [FK_Prestamos_Usuarios]    Script Date: 04/11/2019 11:38:07 ******/
ALTER TABLE [dbo].[Prestamos]  WITH CHECK ADD  CONSTRAINT [FK_Prestamos_Usuarios] FOREIGN KEY([IdUsuario])
REFERENCES [dbo].[Usuarios] ([IdUsuario])
GO
ALTER TABLE [dbo].[Prestamos] CHECK CONSTRAINT [FK_Prestamos_Usuarios]
GO
